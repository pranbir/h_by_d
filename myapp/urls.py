

from django.urls import path
from . import views


urlpatterns = [
    path('login', views.login, name="login"),
    path('register', views.register, name="login"),
    path('secret', views.secret, name="login"),
    path('public', views.public, name="login"),
]
