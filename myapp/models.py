from django.db import models
from django.contrib.auth.models import User


class UserMetaData(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=10, unique=True)