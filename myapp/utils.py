import jwt
from django.http import JsonResponse
from django.conf import Settings, settings
import datetime


def token_checker(func):

    #jwt.decode(encoded_jwt, "secret", algorithms=["HS256"])

    pass


def token_generator(user_obj):
    access_key_payload = {
        "user_id" : user_obj.id,
        "user_name" : user_obj.username,
        "exp": datetime.datetime.now(tz=datetime.timezone.utc) + datetime.timedelta(seconds=settings.JWT_ACCESS_KEY_EXPIRY_TIME)
    }
    refresh_key_payload = {
        "user_id" : user_obj.id,
        "exp": datetime.datetime.now(tz=datetime.timezone.utc) + datetime.timedelta(seconds=settings.JWT_REFRESH_KEY_EXPIRY_TIME)
    }
    encoded_access_key = jwt.encode(access_key_payload, settings.JWT_SECRET, algorithm=settings.JWT_ALGORITHM)
    encoded_refresh_key = jwt.encode(refresh_key_payload, settings.JWT_SECRET, algorithm=settings.JWT_ALGORITHM)
    
    resp = {
        "acces_key" : encoded_access_key,
        "refresh_key" : encoded_refresh_key
        }
    return resp


def refresh_tokens():
    pass


def server_error():
    return JsonResponse({"msg":"Internal server error."}, status=500)


def safe_execute(func):
    def inner(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            return server_error()
    return inner

def custom_response(msg,data=None, code=200):
    resp = {"msg" : msg, "data" : data} if data else  {"msg" : msg}
    return JsonResponse(
           resp, status=code
        )