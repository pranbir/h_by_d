from django.http import JsonResponse
from django.shortcuts import render
from . import utils
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.views.decorators.http import require_http_methods
import json
from .models import UserMetaData
from django.views.decorators.csrf import csrf_exempt



@utils.safe_execute
def login(request):
    user = authenticate(username='john', password='secret')
    if user is not None:
        pass
    else:
        pass



@require_http_methods(["POST"])
@csrf_exempt
@utils.safe_execute
def register(request):
    body_unicode = request.body.decode('utf-8')
    if not body_unicode:
        return utils.server_error()
    body = json.loads(body_unicode)
    user_name = body.get("username")
    password = body.get("password")
    email = body.get("email")
    phone = body.get("phone")

    if not user_name or not password or not email or not phone:
        return utils.custom_response("All Fields are mandatory", code=400)

    if UserMetaData.objects.filter(phone=phone):
        return utils.custom_response("Phone Number already Exists", code=400)
    
    if User.objects.filter(username=user_name):
        return utils.custom_response("Username already Exists", code=400)
    
    user = User.objects.create_user(user_name, email, password)
    user.save()
    user_meta_obj = UserMetaData(user=user, phone=phone)
    user_meta_obj.save()

    tokens = utils.token_generator(user)
    return utils.custom_response("Successfully registered", data=tokens, code=201)


@utils.safe_execute
def secret(request):
     return JsonResponse(
        {
            "data" : "This is SECRET message",
            "status_code" : 200
        }
    )

@utils.safe_execute
def public(request):
    return JsonResponse(
        {
            "data" : "This is Public message",
            "status_code" : 200
        }
    )